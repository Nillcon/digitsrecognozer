from tkinter import *
from tkinter.colorchooser import askcolor
from PIL import Image, ImageDraw, ImageGrab
import numpy as np
from functools import partial
from tempfile import TemporaryFile

import matplotlib.pyplot as plt

from network import NeuralNetwork

width = 400
height = 400
center = height//2

white = (255, 255, 255)

line_width = 10

class Paint(object):
    
    DEFAULT_PEN_SIZE = 5.0
    DEFAULT_COLOR = 'black'

    def __init__(self):
        self.root = Tk()

        self.images = np.empty((0, 28, 28))
        self.labels = np.empty(0)

        self.c = Canvas(self.root, bg='black', width=400, height=400)
        self.c.grid(row=0, columnspan=5)

        self.pen_button = Button(self.root, text='Recognize', font=("Helvetica", 12), command=self.recognize)
        self.pen_button.grid(row=1, column=1)

        self.label = Label(self.root, text="Paint a digit", font=("Helvetica", 15))
        self.label.grid(row=1, column=2)

        self.pen_button = Button(self.root, text='Clear', font=("Helvetica", 12), command=self.clear)
        self.pen_button.grid(row=1, column=3)

        self.network = NeuralNetwork()
        self.outfile = TemporaryFile()

        self.setup()
        self.root.mainloop()

    def setup(self):
        self.old_x = None
        self.old_y = None
        self.line_width = line_width
        self.color = self.DEFAULT_COLOR
        self.eraser_on = False
        # self.active_button = self.pen_button
        self.c.bind('<B1-Motion>', self.paint)
        self.c.bind('<ButtonRelease-1>', self.reset)

    def recognize(self):
        image = self.get_canvas_image()

        answer = self.network.predict(image)
        self.label.config(text=str(answer))

    def clear(self):
        self.c.delete("all")

    def activate_button(self, some_button, eraser_mode=False):
        # self.active_button.config(relief=RAISED)
        some_button.config(relief=SUNKEN)
        self.active_button = some_button
        self.eraser_on = eraser_mode

    def paint(self, event):
        self.line_width = line_width
        paint_color = 'white'
        if self.old_x and self.old_y:
            self.c.create_line(self.old_x, self.old_y, event.x, event.y,
                               width=self.line_width, fill=paint_color,
                               capstyle=ROUND, smooth=TRUE, splinesteps=36)
        self.old_x = event.x
        self.old_y = event.y

    def reset(self, event):
        self.old_x, self.old_y = None, None

    def get_canvas_image(self):
        x = self.c.winfo_rootx() + self.c.winfo_x() + 2
        y = self.c.winfo_rooty() + self.c.winfo_y() + 2
        x1 = x + self.c.winfo_width() - 4
        y1 = y + self.c.winfo_height() - 4

        image = ImageGrab.grab(bbox=(x, y, x1, y1))
        or_image = image.convert('L')

        or_image.thumbnail((28, 28), Image.ANTIALIAS)

        return or_image