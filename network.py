import tensorflow as tf
from tensorflow import keras

import numpy as np

checkpoint_path = "./model.h5"

class NeuralNetwork ():

    def __init__(self):
        self.model = keras.models.load_model(checkpoint_path)

    def study(self, train_images, train_labels, epochs):
        train_images = train_images / 255.0

        self.model.fit(train_images,
            train_labels,
            epochs=epochs)

        self.model.save(checkpoint_path)


    def predict(self, img):
        array_of_image = np.array(img)
        preparate_img = np.expand_dims(array_of_image, 0)

        preparate_img = preparate_img / 255.0

        predict = self.model.predict(preparate_img)

        return np.argmax(predict[0])